import Actors from '../../database/models/Actors.js';

const getAllActorsController = async (req,res) => {
    
    try {
        
        const actors = await Actors.findAll({
            attributes: ['first_name','last_name'],
        });

        res.send({
            status: 'ok',
            data: actors
        });

    } catch (error) {
        console.log(error);
    }
    
}

export default getAllActorsController;