import express from 'express';
import actorRouter from '../src/router/actorRouter.js';

const server = express();

server.use(actorRouter);

export default server;