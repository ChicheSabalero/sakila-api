import express from 'express';
import getAllActorsController from '../controllers/actors/getAllActorsController.js';

const router = express.Router();


router.get('/actors', getAllActorsController);


export default router;