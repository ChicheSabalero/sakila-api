import connection from "../config/configDB.js";
import Actors from "../models/Actors.js";

export const testConnection = async () => {
    try {
        
        await connection.authenticate();
        console.log(`Conectado a base de datos ${connection.getDatabaseName()}`);
    } catch (error) {
        console.log(error);
    }
}

export const syncDB = async () => {

    await Actors.sync({alter: true});
}