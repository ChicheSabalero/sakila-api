import { DataTypes, Model } from "sequelize";
import connection from "../config/configDB.js";

class Actors extends Model {};

Actors.init({
    actor_id:{
        type: DataTypes.SMALLINT.UNSIGNED,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false
    },
    first_name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    last_name:{
        type: DataTypes.STRING,
        allowNull: false
    },
    last_update:{
        type: DataTypes.DATE,
        //defaultValue: null
        allowNull: false
    }
},
{
    tableName: 'actor',
    timestamps: false,
    sequelize: connection    
}
);

export default Actors;
