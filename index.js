import server from "./src/server.js";
import { testConnection, syncDB } from "./src/database/services/servicesDB.js";

const PORT = process.env.PORT || 3001;


testConnection().then(() => {
    syncDB().then(() => {
        server.listen(PORT, () => {
            console.log(`Server escuchando en puerto: ${PORT}`);
        });    
    })
})
